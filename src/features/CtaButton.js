import "../style/features/CtaButton.css";

export default function CtaButton({
  callback = undefined,
  className = "Normal",
  disabled = false,
  title = "Create new",
  type = "button",
}) {
  return (
    <div className={"CtaButton-Container"}>
      <button
        type={type}
        className={"Common-CtaButton " + className}
        onClick={callback}
        disabled={disabled}
      >
        {title}
      </button>
    </div>
  );
}
