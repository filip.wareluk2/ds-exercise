import "../../style/app/App.css";
import "../../features/CtaButton.js";
import CtaButton from "../../features/CtaButton.js";

function App() {
  return (
    <div className="App">
      <Header />
      <div className="App-body">
        <div className="App-body-row">
          <div className="App-body-col">
            <CtaButton
              callback={() => console.log("Pressed Big button")}
              className="Big"
              title="Create New"
            />
          </div>
          <div className="App-body-col">
            <CtaButton className="Big" title="Disabled" disabled />
          </div>
        </div>
        <div className="App-body-row">
          <div className="App-body-col">
            <CtaButton
              callback={() => console.log("Pressed normal button")}
              className="Normal"
              title="Create New"
            />
          </div>
          <div className="App-body-col">
            <CtaButton className="Normal" title="Disabled" disabled />
          </div>
        </div>
        <div className="App-body-row">
          <div className="App-body-col">
            <CtaButton
              callback={() => console.log("Pressed small button")}
              className="Small"
              title="Create New"
            />
          </div>
          <div className="App-body-col">
            <CtaButton className="Small" title="Disabled" disabled />
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

function Header() {
  return (
    <header className="App-header">
      <div className="App-header-container">
        <p className="App-header-title">UI CTA Buttons</p>
      </div>
    </header>
  );
}

function Footer() {
  return (
    <footer className="App-footer">
      <p className="App-footer-text">2022 by F.</p>
    </footer>
  );
}

export default App;
